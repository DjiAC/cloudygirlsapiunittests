<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V2CategoriesControllerTest extends WebTestCase
{
    CONST API_KEY = '8c391b1d74ee2378a500b74f1033c052ab0b5de6b3f61916dec40742dbadf05e';
    // GET CATEGORIES : ceux du prof sont bons
    public function testGetCategoryWithoutAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }
/*
    public function testGetCategoryWithAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category', [], [], ['HTTP_apikey' => V2CategoriesControllerTest::API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category":[{"id":"1","name":"test_category_1"}]}', $client->getResponse()->getContent());
    }
 
    //POST CATEGORIE : LES ID ET NOMS SONT à CHANGER ! 
    public function testPostCategoryWithoutAuth()
    {
        $client = static::createClient();
        $client->request('POST', '/v2/category',['id' => '1'],['name' => 'test_test']);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testPostCategoryWithAuth()
    {
        $client = static::createClient();
        $client->request('POST', '/v2/category', ['id' => '1'],['name'=> 'test_test'], ['HTTP_apikey' => V2CategoriesControllerTest::API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category":[]}', $client->getResponse()->getContent());
    }
    
  
    // GET A CATEGORIE
    public function testGetACategoryWithoutAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/1');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }

    public function testGetACategoryWithoutAuth_404()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/2', [], [], ['HTTP_apikey' => V2CategoriesControllerTest::API_KEY]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Not Found","id":"2"}', $client->getResponse()->getContent());
    }

    public function testGetACategoryWithoutAuth_406()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/la', [], [], ['HTTP_apikey' => V2CategoriesControllerTest::API_KEY]);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"la"}', $client->getResponse()->getContent());
    }

    public function testGetACategoryWithAuth()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/1', [], [], ['HTTP_apikey' => V2CategoriesControllerTest::API_KEY]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"id":1,"name":"test_category_1"}', $client->getResponse()->getContent());
    }
 */  
}

?>
